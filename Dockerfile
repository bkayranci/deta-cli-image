FROM alpine:3.17
LABEL maintainer="Turkalp Burak Kayrancioglu <bkayranci@gmail.com>"

RUN apk add --no-cache curl
RUN curl -fsSL https://get.deta.dev/cli.sh | sh
RUN mv /root/.deta/bin/deta /usr/local/bin
