# deta-cli

The project builds a docker image for [deta-cli](https://github.com/deta/deta-cli) project.


## Gitlab Pipeline Example

### Required Variables

```yaml
- name: DETA_ACCESS_TOKEN
  type: variable
  example: you can generate a token on https://web.deta.sh/settings

- name: DETA_PROG_INFO_FILE
  type: file
  example: {"id":"<redacted>","space":<redacted>,"runtime":"python3.9","name":"<redacted>","path":"<redacted>","project":"<redacted>","account":"<redacted>","region":"eu-central-1","deps":["Authlib==1.2.0","deta==1.1.0","Flask==2.2.2","Flask-Cors==3.0.10","Flask-Login==0.6.2","python-digitalocean==1.17.0"],"envs":["DIGITALOCEAN_CLIENT_ID","DIGITALOCEAN_CLIENT_SECRET","APP_URL","WEB_URL"],"public":true,"log_level":"debug","cron":""}

```

### Pipeline Job

```yaml
deta-deploy:
  stage: deploy
  image: registry.gitlab.com/bkayranci/deta-cli-image:1.3.3
  script:
    - cp $DETA_PROG_INFO_FILE .deta/prog_info
    - deta deploy
  cache:
    paths:
      - .deta/state
  rules:
    - if: $CI_COMMIT_REF_NAME == $CI_DEFAULT_BRANCH

```